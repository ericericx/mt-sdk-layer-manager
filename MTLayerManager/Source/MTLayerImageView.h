//
//  MTLayerImageView.h
//  LayerManagementDemo
//
//  Created by HuangRusty on 2015/4/29.
//  Copyright (c) 2015年 HuangRusty. All rights reserved.
//


@class MTLayerView;

@interface MTLayerImageView : MTLayerView
{
    
}

- (nonnull instancetype) initWithFrame:(CGRect)frame WithImage:(nullable UIImage *)pImg;

@end
