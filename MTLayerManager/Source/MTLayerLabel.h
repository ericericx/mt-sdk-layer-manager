//
//  MTLayerLabel.h
//  LayerManagementDemo
//
//  Created by HuangRusty on 2015/4/29.
//  Copyright (c) 2015年 HuangRusty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MTLayerView;

@interface MTLayerLabel : MTLayerView

- (nonnull instancetype) initWithFrame:(CGRect)frame WithString:(nullable NSString *)pstrText;

@end
